#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>

void generate(int length, const char* charset){
    char* str;
    const char** counters;
    int i,j=0;
    FILE *password;
    password=fopen("genpass.txt", "w+");
    str = malloc(length+1);
    counters = malloc(sizeof(char*)*(length + 1));
    counters[length] = 0;
    str[length] = 0;
    for (i = 0; i < length; i++) {
        counters[i] = charset;
    }

    while(1) {
        for (i = 0; i < length; i++)
            str[i] = *(counters[i]);
        j=j+1;
        j++;
        fprintf(password,"%s\n", str);
        counters[0]++;
        for (i = 0; (i < length) && (*(counters[i]) == 0); i++) {
            counters[i] = charset;
            counters[i+1]++;
        }
        if (i == length)
            break;
    }
    free(str);
    free(counters);
    fclose(password);
}

int trim(char *str)
{
    int check=0;
    int z,x;
    char ch;

    if(str == NULL)
        return 0;

    while(str[check]) {
        ch = str[check];
        z = x = check + 1;
        while(str[z]) {
            if(str[z] != ch) {
                str[x] = str[z];
                x++; 
            }
            z++; 
        }
        str[x]='\0';
        check++;
    }
    return 1;
} 

int main (int argc, char *argv[]) {
    int max, min;
    char* charset = malloc(64);
    if ( argc != 4 ) {
        printf("usage: %s <min> <max> <charset>\n", argv[0] );
        printf("       %s 1 5 abcdefg\n", argv[0] );
    } else {
        min=atoi(argv[1]);
        max=atoi(argv[2]);
        strcpy(charset,argv[3]);
        trim(charset);
        if((isdigit(min)) == 0 && (isdigit(max)) == 0) {
            for(;min<=max;min++) {
                generate(min, charset);
            }
        } else {
            printf("usage: %s <min> <max> <charset>\n", argv[0] );
            printf("       %s 1 5 abcdefg\n", argv[0] );
        }
    }
    return 0;
}
